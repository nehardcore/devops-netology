# devops-netology
**/.terraform/* - исключить все файлы скрытой директории .terraform, вне зависимости от родительской директории (но under .\devops-netology\terraform\)

*.tfstate - исключить все файлы с расширением tfstate
*.tfstate.* - исключить все файлы, расширение которого нчинается с tfstate

crash.log - исключить файл crash.log
crash.*.log - исключить файлы с именем начинающимся с crash, расширением log

*.tfvars - исключить все файлы с расширением tfvars

override.tf - исключить файл override.tf
override.tf.json - исключить файл override.tf.json
*_override.tf - исключить файлы заканчивающиеся на _override.tf
*_override.tf.json - исключить файлы заканчивающиеся на _override.tf.json

.terraformrc - исключить файлы с расширением .terraformrc
terraform.rc - исключить файл terraform.rc